#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST_ */
/*=======================================================*/
class CKnife;
class CClothes;
class CShoes;
class CMobile;
/*=======================================================*/
class CNecessity {
public:
	CNecessity() {}
	virtual ~CNecessity() {}
	virtual CNecessity * clone() const = 0;
	//-----------------------------------------------
	virtual int Weight() const = 0;
	virtual string Name() const = 0;
	//-----------------------------------------------
	friend ostream& operator <<(ostream &os, const CNecessity &a) {
		a.Print(os);
		return os;
	}
	virtual void Print(ostream &os) const = 0;
	//-----------------------------------------------
	virtual bool IsDangerous() const { return false; }
	//-----------------------------------------------
	bool operator <(CNecessity &a) const {
		return compare1(a);
	}
	bool operator <(const  CNecessity *a) const {
		return compare1(*a);
	}
	bool operator ==(const CNecessity &a) const {
		return equal1(a);
	}
	bool operator !=(const CNecessity &a) const {
		return !equal1(a);
	}
	//-----------------------------------------------
	virtual bool compare1(const CNecessity &a) const = 0;
	virtual bool equal1(const CNecessity &a) const = 0;
	//-----------------------------------------------
	virtual bool compare2(const CKnife *a) const { return false; }
	virtual bool compare2(const CClothes *a) const { return false; }
	virtual bool compare2(const CShoes *a) const { return false; }
	virtual bool compare2(const CMobile *a) const { return false; }
	//-----------------------------------------------
	virtual bool equal2(const CKnife *a) const { return false; }
	virtual bool equal2(const CClothes *a) const { return false; }
	virtual bool equal2(const CShoes *a) const { return false; }
	virtual bool equal2(const CMobile *a) const { return false; }
	//-----------------------------------------------
protected:
};
/*=======================================================*/
class CKnife : public CNecessity
{
public:
	CKnife(int bladeLength) :m_lenght(bladeLength) {}
	//-----------------------------------------------
	virtual CNecessity * clone() const
	{
		return (new CKnife(*this));
	}
	//-----------------------------------------------
	virtual bool IsDangerous() const {
		return (m_lenght > 7);
	}
	//-----------------------------------------------
	virtual string Name()const { return m_name; }
	virtual int Weight() const { return M_WEIGHT; }
	//int Lenght() const { return m_lenght; }
	//-----------------------------------------------
	virtual void Print(ostream &os) const {
		os << m_name << ", blade: " << m_lenght << " cm" << endl;
	}
	//-----------------------------------------------
	virtual bool compare1(const CNecessity &a) const {
		if (this->Weight() == a.Weight())
			return !a.compare2(this);
		return this->Weight() < a.Weight();
	}
	virtual bool compare2(const CKnife *a) const {
		return m_lenght < a->m_lenght;
	}
	//-----------------------------------------------
	virtual bool equal1(const CNecessity &a) const {
		return a.equal2(this);
	}
	virtual bool equal2(const CKnife *a) const {
		return (M_WEIGHT == a->M_WEIGHT && m_lenght == a->m_lenght);
	}
	//-----------------------------------------------
private:
	int m_lenght;
	int M_WEIGHT = 100;
	string m_name = "Knife";
};
//---------------------------------------------
class CClothes : public CNecessity
{
public:
	CClothes(string desc) :m_descr(desc)
	{}
	//-----------------------------------------------
	virtual CNecessity * clone() const
	{
		return (new CClothes(*this));
	}
	//-----------------------------------------------
	virtual string Name() const { return m_name; }
	virtual int Weight() const { return M_WEIGHT; }
	//-----------------------------------------------
	virtual void Print(ostream &os) const {
		os << m_name << " (" << m_descr << ")" << endl;
	}
	//-----------------------------------------------
	virtual bool compare1(const CNecessity &a) const {
		if (this->Weight() == a.Weight())
			return !a.compare2(this);
		return this->Weight() < a.Weight();
	}
	virtual bool compare2(const CClothes *a) const {
		return m_descr < a->m_descr;
	}
	//-----------------------------------------------
	virtual bool equal1(const CNecessity &a) const {
		return a.equal2(this);
	}
	virtual bool equal2(const CClothes *a) const {
		return (M_WEIGHT == a->M_WEIGHT && m_descr == a->m_descr);
	}
	//-----------------------------------------------
private:
	string m_descr;
	int M_WEIGHT = 500;
	string m_name = "Clothes";
};
//---------------------------------------------
class CShoes : public CNecessity
{
public:
	CShoes() {}
	//-----------------------------------------------
	virtual CNecessity * clone() const
	{
		return (new CShoes(*this));
	}
	//-----------------------------------------------
	virtual string Name() const { return m_name; }
	virtual int Weight() const { return M_WEIGHT; }
	//-----------------------------------------------
	virtual void Print(ostream &os) const {
		os << m_name << endl;
	}
	//-----------------------------------------------
	virtual bool compare1(const CNecessity &a) const {
		if (this->Weight() == a.Weight())
			return !a.compare2(this);
		return this->Weight() < a.Weight();
	}
	virtual bool compare2(const CShoes *a) const {
		return false;
	}
	//-----------------------------------------------
	virtual bool equal1(const CNecessity &a) const {
		return a.equal2(this);
	}
	virtual bool equal2(const CShoes *a) const {
		return true;
	}
	//-----------------------------------------------
private:
	int M_WEIGHT = 750;
	string m_name = "Shoes";
};
//---------------------------------------------
class CMobile : public CNecessity
{
public:
	CMobile(string manufacturer, string model) : m_manufacturer(manufacturer), m_model(model)
	{};
	//-----------------------------------------------
	virtual CNecessity * clone() const
	{
		return (new CMobile(*this));
	}
	//-----------------------------------------------
	virtual bool IsDangerous() const {
		return (m_manufacturer == "Samsung" && m_model == "Galaxy Note S7");
	}
	//-----------------------------------------------
	virtual string Name() const { return m_name; }
	virtual int Weight() const { return M_WEIGHT; }
	//-----------------------------------------------
	virtual void Print(ostream &os) const {
		os << m_name << " " << m_model << " by: " << m_manufacturer << endl;
	}
	//-----------------------------------------------
	virtual bool compare1(const CNecessity &a) const {
		if (this->Weight() == a.Weight())
			return !a.compare2(this);
		return this->Weight() < a.Weight();
	}
	virtual bool compare2(const CMobile *a) const {
		return (m_manufacturer< a->m_manufacturer
			|| (m_manufacturer == a->m_manufacturer && m_model<a->m_model));
	}
	//-----------------------------------------------
	virtual bool equal1(const CNecessity &a) const {
		return a.equal2(this);
	}
	virtual bool equal2(const CMobile *a) const {
		return (M_WEIGHT == a->M_WEIGHT && m_manufacturer == a->m_manufacturer && m_model == a->m_model);
	}
	//-----------------------------------------------
private:
	string m_manufacturer;
	string m_model;
	int M_WEIGHT = 150;
	string m_name = "Mobile";
};
/*=======================================================*/
class CBackpack
{
public:
	CBackpack() :m_count(0), m_total_weight(1000) {}
	~CBackpack() {}
	//-----------------------------------------
	virtual CBackpack& Add(const CNecessity &a) {
		//CNecessity *tmp = a.clone();
		m_necessities.push_back(shared_ptr<CNecessity>(a.clone()));
		m_total_weight += a.Weight();
		m_count++;
		//delete tmp;
		return (*this);
	}
	//-----------------------------------------
	virtual int Weight() const {
		return m_total_weight;
	}
	//-----------------------------------------
	virtual int Count() const {
		return m_count;
	}
	//-----------------------------------------
	virtual bool Danger() const {
		for (auto i = m_necessities.begin(); i != m_necessities.end(); i++)
		{
			if ((*i)->IsDangerous()) return true;
		}
		return false;
	}
	//-----------------------------------------
	virtual bool IdenticalContents(const CBackpack &x) const;
	//-----------------------------------------
	friend ostream& operator <<(ostream &os, const CBackpack &a) {
		a.Print(os);
		return os;
	}
	//-----------------------------------------
	virtual void Print(ostream& os) const {
		os << "Backpack\n";
		for (auto it = m_necessities.begin(); it != m_necessities.end(); it++)
		{
			if (distance(it, m_necessities.end()) == 1)
				os << "\\-";
			else
				os << "+-";
			(*it)->Print(os);
		}
	}
protected:
	vector<shared_ptr<CNecessity>>m_necessities;
	int m_count;
	int m_total_weight;
};
/*================================================================================*/
class CSuitcase : public CBackpack
{
public:
	CSuitcase(int w, int h, int d) :CBackpack(), m_w(w), m_h(h), m_d(d)
	{
		m_total_weight = 2000;
	}
	virtual void Print(ostream& os) const {
		os << "Suitcase " << m_w << "x" << m_h << "x" << m_d << endl;
		for (auto it = m_necessities.begin(); it != m_necessities.end(); it++)
		{
			if (distance(it, m_necessities.end()) == 1)
				os << "\\-";
			else
				os << "+-";
			(*it)->Print(os);
		}
	}
private:
	int m_w;
	int m_h;
	int m_d;
};
/*=======================================================*/
bool CBackpack::IdenticalContents(const CBackpack &x) const {
	if (this->m_necessities.size() != x.m_necessities.size())
		return false;
	for (auto it1 = m_necessities.begin(); it1 != m_necessities.end(); it1++)
	{
		bool check = false;
		for (auto it2 = x.m_necessities.begin(); it2 != x.m_necessities.end(); it2++)
		{
			if (**it1 == **it2)
			{
				check = true;
				break;
			}
		}
		if (!check)
			return false;
	}
	return true;
}
/*=======================================================*/
#ifndef __PROGTEST__
int main(void)
{
	cout << boolalpha;
	cout << is_polymorphic<CBackpack>::value << '\n';
	cout << is_polymorphic<CSuitcase>::value << '\n';
	cout << is_polymorphic<CNecessity>::value << '\n';
	CSuitcase x(1, 2, 3);
	CBackpack y;
	ostringstream os;
	x.Add(CKnife(7));
	x.Add(CClothes("red T-shirt"));
	x.Add(CClothes("black hat"));
	x.Add(CShoes());
	x.Add(CClothes("green pants"));
	x.Add(CClothes("blue jeans"));
	x.Add(CMobile("Samsung", "J3"));
	x.Add(CMobile("Tamtung", "Galaxy Note S7"));
	os.str("");
	os << x;
	//cout << os.str();

	assert(os.str() == "Suitcase 1x2x3\n"
		"+-Knife, blade: 7 cm\n"
		"+-Clothes (red T-shirt)\n"
		"+-Clothes (black hat)\n"
		"+-Shoes\n"
		"+-Clothes (green pants)\n"
		"+-Clothes (blue jeans)\n"
		"+-Mobile J3 by: Samsung\n"
		"\\-Mobile Galaxy Note S7 by: Tamtung\n");
	//cout << "********************" << endl;
	assert(x.Count() == 8);
	assert(x.Weight() == 5150);
	assert(!x.Danger());
	x.Add(CKnife(8));
	//	cout << x;
	os.str("");
	os << x;
	assert(os.str() == "Suitcase 1x2x3\n"
		"+-Knife, blade: 7 cm\n"
		"+-Clothes (red T-shirt)\n"
		"+-Clothes (black hat)\n"
		"+-Shoes\n"
		"+-Clothes (green pants)\n"
		"+-Clothes (blue jeans)\n"
		"+-Mobile J3 by: Samsung\n"
		"+-Mobile Galaxy Note S7 by: Tamtung\n"
		"\\-Knife, blade: 8 cm\n");
	//cout << "********************" << endl;
	assert(x.Count() == 9);
	assert(x.Weight() == 5250);
	assert(x.Danger());
	y.Add(CKnife(7))
		.Add(CClothes("red T-shirt"))
		.Add(CShoes());
	y.Add(CMobile("Samsung", "Galaxy Note S7"));
	y.Add(CShoes());
	y.Add(CClothes("blue jeans"));
	y.Add(CClothes("black hat"));
	y.Add(CClothes("green pants"));
	//cout << y;
	os.str("");
	os << y;
	assert(os.str() == "Backpack\n"
		"+-Knife, blade: 7 cm\n"
		"+-Clothes (red T-shirt)\n"
		"+-Shoes\n"
		"+-Mobile Galaxy Note S7 by: Samsung\n"
		"+-Shoes\n"
		"+-Clothes (blue jeans)\n"
		"+-Clothes (black hat)\n"
		"\\-Clothes (green pants)\n");
	//cout << "********************" << endl;
	assert(y.Count() == 8);
	assert(y.Weight() == 4750);
	assert(y.Danger());
	y.Add(CMobile("Samsung", "J3"));
	y.Add(CMobile("Tamtung", "Galaxy Note S7"));
	y.Add(CKnife(8));
	//cout << y;
	os.str("");
	os << y;
	assert(os.str() == "Backpack\n"
		"+-Knife, blade: 7 cm\n"
		"+-Clothes (red T-shirt)\n"
		"+-Shoes\n"
		"+-Mobile Galaxy Note S7 by: Samsung\n"
		"+-Shoes\n"
		"+-Clothes (blue jeans)\n"
		"+-Clothes (black hat)\n"
		"+-Clothes (green pants)\n"
		"+-Mobile J3 by: Samsung\n"
		"+-Mobile Galaxy Note S7 by: Tamtung\n"
		"\\-Knife, blade: 8 cm\n");
	//cout << "********************" << endl;
	assert(y.Count() == 11);
	assert(y.Weight() == 5150);
	assert(y.Danger());
	assert(!x.IdenticalContents(y));
	assert(!y.IdenticalContents(x));
	x.Add(CMobile("Samsung", "Galaxy Note S7"));
	assert(!x.IdenticalContents(y));
	assert(!y.IdenticalContents(x));
	x.Add(CShoes());
	assert(x.IdenticalContents(y));
	assert(y.IdenticalContents(x));
	assert(y.IdenticalContents(y));
	assert(x.IdenticalContents(x));


	//system("pause");
	return 0;
}
#endif /* __PROGTEST__ */
